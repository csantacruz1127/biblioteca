from django.db import models
from model_utils.models import TimeStampedModel

#import biblio app
from applications.biblio.models import Libro

class Actividad(TimeStampedModel):
    """ Modelo para actividades del circulo de lectores """

    libro = models.ForeignKey(Libro, on_delete=models.CASCADE)
    titulo = models.CharField('Titulo de Actividad', max_length=100)
    descripcion = models.TextField()
    fecha = models.DateField()

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'

    def __str__(self):
        return 'ID ' + str(self.id) + ' - ' + str(self.titulo)


class Participante(TimeStampedModel):
    """ Modelo para particoantes de una actividad """

    actividad = models.ForeignKey(Actividad, on_delete=models.CASCADE)
    email = models.EmailField('Correo')
    phone = models.CharField('Celular', max_length=20)
    full_name = models.CharField('Nombres', max_length=50)

    class Meta:
        verbose_name = 'Participante'
        verbose_name_plural = 'Participantes'

    def __str__(self):
        return 'ID ' + str(self.id) + ' - ' + str(self.full_name)