from django.db import models
from model_utils.models import TimeStampedModel

class Autor(TimeStampedModel):
    """ Modelo para autores """

    nombres = models.CharField('Nombres completos:', max_length=60)
    nacionalidad = models.CharField('Nacionalidad', max_length=20)
    seudonimo = models.CharField('Seudonimo', max_length=15)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return 'ID ' + str(self.id) + ' - ' + str(self.nombres)


class Libro(TimeStampedModel):
    """ Modelo para Libros de un autor """

    ESTADO_CHOICES = (
        ('0', 'En Curso'),
        ('1', 'En Leido'),
        ('2', 'Por Leer'),
    )

    titulo = models.CharField('Titulo del libro', max_length=100)
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    portada = models.ImageField(upload_to='Libro')
    estado = models.CharField(
        'Estado',
        max_length=2,
        choices=ESTADO_CHOICES
    )

    class Meta:
        verbose_name = 'Libro'
        verbose_name_plural = 'Libros'

    def __str__(self):
        return 'ID ' + str(self.id) + ' - ' + str(self.titulo)