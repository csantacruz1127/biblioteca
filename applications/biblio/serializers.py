from rest_framework import serializers, pagination
from .models import (
    Libro,
    Autor
)
class LibroSerializer(serializers.ModelSerializer):
    """ serializador para libors """
    
    class Meta:
        model = Libro
        fields = (
            'id',
            'titulo',
            'autor',
            'portada',
            'estado',
        )