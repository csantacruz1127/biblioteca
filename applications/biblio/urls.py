# django
from django.urls import include, re_path, path

# local
from . import views

app_name="providers_app"

urlpatterns = [
    # lista las 6 categorias principales
    path(
        'libros/leidos-semana/',
        views.LibrosLeidos.as_view(),
        name='libro_leidos'
    ),
]