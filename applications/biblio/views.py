from django.shortcuts import render
#
from rest_framework import viewsets, generics
#
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    UpdateAPIView
)

from .models import (
    Libro,
    Autor
)
#
from .serializers import (
    LibroSerializer
)

class LibrosLeidos(ListAPIView):
    """
        Lista leidos esta semana
    """
    serializer_class = LibroSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        # 6 categorias principales
        return Libro.objects.filter(
            estado='1'
        )