import Vue from 'vue'
import App from './App.vue'
// for foundation css
import 'foundation-sites/dist/css/foundation.css'

// local settings
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
