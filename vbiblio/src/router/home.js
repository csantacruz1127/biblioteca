// importando compnnentes locales
import Index from '@/views/Index'
import Home from '@/views/Home'
import Autores from '@/views/Autores'
import Actividades from '@/views/Actividades'
import Participar from '@/views/Participar'

const homeRutes = [
  {
    path: '',
    component: Index,
    children: [
      {
        path: '',
        name: 'home',
        component: Home
      },
      {
        path: '/autores',
        name: 'autores',
        component: Autores
      },
      {
        path: '/actividades',
        name: 'actividades',
        component: Actividades
      },
      {
        path: '/participar',
        name: 'participar',
        component: Participar
      }
    ]
  }
]
export default homeRutes