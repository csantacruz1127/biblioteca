import Vue from 'vue'
import Router from 'vue-router'
// archivo de rutas locales
import homeRutes from './home'
// vuex state user

Vue.use(Router)

const rutas = new Router({
  routes: homeRutes,
  mode: 'history'
})


export default rutas